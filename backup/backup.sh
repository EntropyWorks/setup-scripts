#!/bin/bash
NOW=$(date +"%m-%d-%Y")
FILE="config-backup.$NOW.tar.bz"

clear
echo
echo
echo "This script will backup the files and directories listed in $HOME/setup-scripts/backup/backup-list.txt"
echo
echo "The resulting compressed .tar.bz file can be used to restore most of your system's configuration settings after a fresh install."
echo 
echo "Would you like to continue?"
echo
select yn in "Yes" "No"; do
	case $yn in
		Yes ) 
			tar -cvjf $FILE -T $HOME/setup-scripts/backup/list-backup.txt
			break;;

		No ) break;;
	esac
done
