# Tri-Mode Setup Changelog

# Version 0.6.1
 - Changed to pull Corps icon from local instead of the web.

# Version 0.6
 - Added "Easy Button" for quick setup of USB controlled radios with FLRIG control.

# Version 0.5.3
 - Added FTDX-3000 support

# Version 0.5.2
 - Initial release
