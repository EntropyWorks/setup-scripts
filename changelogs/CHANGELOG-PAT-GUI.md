# PAT-GUI Changelog

# Version 0.5.2
 - Changed to pull Corps icon from local instead of the web.

# Version 0.5.1
 - Initial release
