#!/bin/bash
 ################################################################################
 ################################################################################
 #######################  AmRRON Tri-Mode Setup v0.6.1  #########################
 #######################        Created by TB-14        #########################
 #######################          20 Dec 2019           #########################
 ################################################################################
 ################################################################################


### This script will perform basic setup for TriMode Operation.


function set_screen_size() {
screen_size=$(xdpyinfo | grep dimensions | \
sed -r 's/^[^0-9]*([0-9]+x[0-9]+).*$/\1/' | sed s/x/" "/ || echo 800 480)
width=$(echo $screen_size | awk '{print $1}')
height=$(echo $screen_size | awk '{print $2}')
#xdpyinfo  | grep 'dimensions:'
# Divide by two so the dialogs take up half of the screen, which looks nice.
rows=$(( height / 2 ))
columns=$(( width / 2 ))
# Unless the screen is tiny
rows=$(( rows < 400 ? 400 : rows ))
columns=$(( columns < 700 ? 700 : columns ))
c="--width=${columns}"
r="--height=${rows}"
}

function determine_os() {
#Deterimine computer type.
function noOS_Support() {
	yad --image=$DIR/images/corps-icon.png --center --no-buttons \
		--title "INVALID OS DETECTED" \
		--text "A valid OS was not detected.\n\nThis installer was designed for \
Debian based distros; specifically Raspbian Stretch, Linux Mint 18.3 / 19.1 and Ubuntu 18.04." \
		${r} ${c}
		exit 1

}

function maybeOS_Support() {
	if (yad --image=$DIR/images/corps-icon.png --center \
	--title "Not Supported OS" \
	--text "Your OS may be compatible with this installer but has not been \
tested.\n\nThis installer has been tested on Raspbian Stretch, Ubuntu \
18.04, and Mint 18.3 / 19.1.\n\nWould you like to continue anyway?" ${r} ${c}) then
	    yad --image=$DIR/images/corps-icon.png --center \
	    --title "Not Supported OS" \
	    --text "::: Did not detect perfectly supported OS but,\n\n \
::: Continuing installation at user's own risk..." ${r} ${c} ||
	    { echo "exiting"; exit 1; }
	else
	    yad --image=$DIR/images/corps-icon.png --center \
	    --title "Not Supported OS" \
	    --text "::: Exiting due to unsupported OS" ${r} ${c}
	    exit 1
	fi
}

# Determine location user is running script from.
SOURCE="${BASH_SOURCE[0]}"
     # While $SOURCE is a symlink, resolve it
     while [ -h "$SOURCE" ]; do
          DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
          SOURCE="$( readlink "$SOURCE" )"
          # If $SOURCE was a relative symlink (so no "/" as prefix, need to
          #resolve it relative to the symlink base directory
          [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE"
     done
     DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
     #echo "$DIR"



# Compatibility check for OS
  # if lsb_release command is on their system
  if hash lsb_release 2>/dev/null; then

    PLAT=$(lsb_release -si)
    OSCN=$(lsb_release -sc) # We want this to be trusty xenial bionic cosmic disco; jessie strech buster; sara serena sonya sylvia tara tessa tina

  else # else get info from os-release

    PLAT=$(grep "^NAME" /etc/os-release | awk -F "=" '{print $2}' | tr -d '"' \
    | awk '{print $1}')
    VER=$(grep "VERSION_ID" /etc/os-release | awk -F "=" '{print $2}' | tr -d '"')
    declare -A VER_MAP=(["10"]="buster" ["9"]="stretch" ["8"]="jessie" ["16.04"]="xenial" \
    ["14.04"]="trusty" ["18.04"]="bionic" ["18.10"]="cosmic" ["19.04"]="disco" ["18"]="sara" ["18.1"]="serena" \
    ["18.2"]="sonya" ["18.3"]="sylvia" ["19"]="tara" ["19.1"]="tessa" ["19.2"]="tina" )
    OSCN=${VER_MAP["${VER}"]}

  fi

  case ${PLAT} in
    Ubuntu|Raspbian|Debian|LinuxMint)
      case ${OSCN} in
        trusty|xenial|bionic|cosmic|disco|jessie|stretch|buster|sara|serena|sonya|sylvia|tara|tessa|tina)
          ;;
        *)
          maybeOS_Support
          ;;
      esac
      ;;
    *)
      noOS_Support
      ;;
  esac
}

#create a finish trap
function finish {
  if [[ -f /tmp/trimode-selections ]]; then
rm /tmp/trimode-selections
fi

if [[ -f /tmp/trimode ]]; then
rm /tmp/trimode
fi

if [[ -f /tmp/trimode-rts ]]; then
rm /tmp/trimode-rts
fi

if [[ -f /tmp/trimode-cat ]]; then
rm /tmp/trimode-cat
fi

if [[ -f /tmp/banner.png ]]; then
rm /tmp/banner.png
fi

}
trap finish EXIT

#Lets grab the AmRRON Banner.
#wget -N http://s9756.storage.proboards.com/5619756/a/KWTGc2YscxSclVEeB5jD.png -O /tmp/banner.png

##Some Variables used by more than one function
ARDOPSTATUS="/usr/local/bin/ardop-status"
RADIOS=$(echo Not-Listed,FT-817,FT-857,FT-891,FT-897,FT-990,FTDX-3000,IC-706Mk2G,IC-718,IC-7100,IC-7200,IC-7300,IC-7410,KX-2,KX-3)
RTSOPTIONS="$(ls /dev/ | grep tty[A-Z] | grep -v ttyS[0-9] | tr '\n' ',')"
if [[ $RTSOPTIONS == "" ]];then
	RTSOPTIONS="ttyUSB0"
fi

function ardop_launcher() {
local USERDIR=$(dirname $ARDOPLOC)

if [[ ${PLAT} = "Raspbian" ]]; then
	local var1="piardopc"
	local var13="piARDOP_GUI"
	else
	local var1="ardopc"
	local var13="ARDOP_GUI"
fi
local var2='$(pidof '"$var1"')'
local var3='$(pidof pat)'
local var14='$(pidof '"$var13"')'
local var4='$ardopstat'
local var5='$ardoppid'
local var6='$patstat'
local var7='$patpid'
local var15='$guistat'
local var16='$guipid'
local var8='$(cat /tmp/process-status | sed -n '"'"3p"'"' | awk -F "|" '"'"'{print $1}'"'"')'
local var9='$(cat /tmp/process-status | sed -n '"'"5p"'"' | awk -F "|" '"'"'{print $1}'"'"')'
local var17='$(cat /tmp/process-status | sed -n '"'"4p"'"' | awk -F "|" '"'"'{print $1}'"'"')'
local var10='${ARDOPRESULTS}'
local var11='${PATRESULTS}'
local var18='${GUIRESULTS}'
local var12=$USERDIR
local var19='$(pidof pavucontrol)'
local var20='$pavustat'
local var21='$pavupid'
local var22='$(cat /tmp/process-status | sed -n '"'"1p"'"' | awk -F "|" '"'"'{print $1}'"'"')'
local var23='${PAVURESULTS}'
local var24='$(pidof rigctld)'
local var25='$rigstat'
local var26='$rigpid'
local var27='$(cat /tmp/process-status | sed -n '"'"2p"'"' | awk -F "|" '"'"'{print $1}'"'"')'
local var28='${RIGRESULTS}'
local var29='$(pidof socat)'


cat > /tmp/ardop-status <<EOF
#!/bin/bash
#TRIMODE


#create a finish trap
function finish {
  if [[ -f /tmp/process-status ]]; then
rm /tmp/process-status
fi

}
trap finish EXIT


while true
do



if pidof $var1 2>/dev/null; then
ardopstat="TRUE"
ardoppid=$var2
else
ardopstat="FALSE"
ardoppid="None"
fi

if pidof $var13 2>/dev/null; then
guistat="TRUE"
guipid=$var14
else
guistat="FALSE"
guipid="None"
fi

if pidof pat 2>/dev/null; then
patstat="TRUE"
patpid=$var3
else
patstat="FALSE"
patpid="None"
fi

if pidof pavucontrol 2>/dev/null; then
pavustat="TRUE"
pavupid=$var19
else
pavustat="FALSE"
pavupid="None"
fi

if pidof rigctld 2>/dev/null; then
rigstat="TRUE"
rigpid=$var24
else
rigstat="FALSE"
rigpid="None"
fi

if ! hash pat 2>/dev/null; then
patstat="NOT_INSTALLED"
patpid="NOT_INSTALLED"
fi

if ! hash pavucontrol 2>/dev/null; then
pavustat="NOT_INSTALLED"
pavupid="NOT_INSTALLED"
fi

if ! hash rigctl 2>/dev/null; then
rigstat="NOT_INSTALLED"
rigpid="NOT_INSTALLED"
fi



yad --center --checklist --list --print-all \
--title=Status \
--button=Close:1 --button=Toggle:0 \
--column=Active --column=Name --column=PID  \
$var20 PAVUCONTROL $var21 \
$var25 RIGCTLD $var26 \
$var4 ARDOPC $var5 \
$var15 ARDOP_GUI $var16 \
$var6 PAT $var7 \
--width=250 --height=250 >> /tmp/process-status || exit 0



ARDOPRESULTS=$var8
PATRESULTS=$var9
GUIRESULTS=$var17
PAVURESULTS=$var22
RIGRESULTS=$var27

if ( [[ $var4 == TRUE ]] && [[ $var10 == FALSE ]] ); then
kill $var5
fi

if ( [[ $var4 == FALSE ]] && [[ $var10 == TRUE ]] ); then
$ARDOPCMD
fi

if ( [[ $var6 == TRUE ]] && [[ $var11 == FALSE ]] ); then
kill $var7
fi

if ( [[ $var6 == FALSE ]] && [[ $var11 == TRUE ]] ); then
pat http &
fi

if ( [[ $var15 == TRUE ]] && [[ $var18 == FALSE ]] ); then
kill $var16
fi

if ( [[ $var15 == FALSE ]] && [[ $var18 == TRUE ]] ); then
$var12/$var13 &
fi

#########
#rigctl
if ( [[ $var25 == TRUE ]] && [[ $var28 == FALSE ]] ); then
kill $var26
kill $var29
fi

if ( [[ $var25 == FALSE ]] && [[ $var28 == TRUE ]] ); then
$RIGCTLCMD
fi

#pavucontrol
if ( [[ $var20 == TRUE ]] && [[ $var23 == FALSE ]] ); then
kill $var21
fi

if ( [[ $var20 == FALSE ]] && [[ $var23 == TRUE ]] ); then
/usr/bin/pavucontrol &
fi

finish
sleep 2

done

EOF

chmod +x /tmp/ardop-status
sudo mv /tmp/ardop-status /usr/local/bin/ardop-status
}


function find_rig() {
RADIOMK=$(rigctl -l | awk '{print $2}' |  awk '!x[$0]++' | grep -v "#" | tr '\n' ',')
CUSTOMRAD1=$(yad --center --wrap --title="Rigctl Setup" --text="Please answer the following selections:" \
--image=$DIR/images/corps-icon.png \
--form --separator="," --item-separator="," \
--field="Select Radio Make::CB" $RADIOMK ${c} ${r} | tr -d ,) || exit 1

RADIOMOD=$(rigctl -l | grep $CUSTOMRAD1 | awk '{print $3}' | tr '\n' ',')
CUSTOMRAD2=$(yad --center --wrap --title="Rigctl Setup" --text="Please answer the following selections:" \
--image=$DIR/images/corps-icon.png \
--form --separator="," --item-separator="," \
--field="Select Radio Model::CB" $RADIOMOD ${c} ${r} | tr -d ,) || exit 1

RADIOID=$(rigctl -l | grep $CUSTOMRAD2 | awk '{print $1}')
CUSTOMRAD3=$(yad --center --wrap --title="Rigctl Setup" --text="Please answer the following selections:" \
--image=$DIR/images/corps-icon.png \
--form --separator="," --item-separator="," \
--field="Rigctl Radio ID::CBE" $RADIOID \
--field="Tip::TXT" "Per your selections, this should be your Radio ID for rigctl.  If you know it to be different, type the correct value instead." ${c} ${r}) || exit 1
RID=$(echo $CUSTOMRAD3 | awk -F "," '{print $1}')


}



function rigctl_setup() {

RIGCTLOPTIONS=$(yad --center --wrap --title="Rigctl Setup" --text="Please answer the following selections:" \
--image=$DIR/images/corps-icon.png \
--form --separator="," --item-separator="," \
--field="PTT Device::CBE" $RTSOPTIONS \
--field="Select Radio::CB" $RADIOS \
--field="Enter Baud Rate" "19200" \
--field="Tip::TXT" "If you radio is not listed, Select your PTT Device and Baud Rate then select 'Not Listed' from the Radio menu." ${c} ${r}) || exit 1

RIGDEV=$(echo $RIGCTLOPTIONS | awk -F "," '{print $1}')
RADIO=$(echo $RIGCTLOPTIONS | awk -F "," '{print $2}')
BAUD=$(echo $RIGCTLOPTIONS | awk -F "," '{print $3}')

case $RADIO in

Not-Listed)
find_rig
;;

FT-817)
RID="120"
;;
FT-857)
RID="122"
;;
FT-891)
RID="136"
;;
FT-897)
RID="123"
;;
FT990)
RID="116"
;;
FTDX-3000)
RID="132"
;;
IC-706Mk2G)
RID="311"
;;
IC-718)
RID="313"
;;
IC-7100)
RID="370"
;;
IC-7200)
RID="361"
;;
IC-7300)
RID="373"
;;
IC-7410)
RID="367"
;;
KX-2|KX-3)
RID="229"
;;
esac


RIGCTLCMD="rigctld -vvvvv -m $RID -r /dev/$RIGDEV -s $BAUD &"
if [[ $SOCATRIGCTL == "True" ]]; then
    RIGCTLCMD="socat pty,link=/tmp/rigctl,waitslave,b38400 tcp:localhost:4532,retry &
rigctld -vvvvv -m $RID -r /dev/$RIGDEV -s $BAUD &"
fi

}


function create_asound() {

cat > /tmp/trimode <<EOF

pcm.ARDOP {
type rate
slave {
pcm "plughw:1,0"
rate 48000
}
}

pcm.pulse {
type pulse
}

ctl.pulse {
type pulse
}

EOF
if [[ -f /etc/asound.conf ]]; then
sudo cp /etc/asound.conf /etc/asound.conf.bak
fi
sudo cp /tmp/trimode /etc/asound.conf
}

function status_update() {
#If they selected to return to stock..
if [[ $STOCK == Yes ]]; then
	#And the ardop-status program exists...
if [[ -f $ARDOPSTATUS ]]; then
	#We escape out the / from location name so it will work with sed and replace the modified ardop command with the basic ardopc & command.  We also remove the socat line from rigctld launcher.
ARDOPLOC=$(echo $ARDOPLOC | sed -e 's|/|\\/|g')
sudo sed -i "/$ARDOPLOC/c\\$ARDOPLOC &" $ARDOPSTATUS
sudo sed -i '/^socat/d' $ARDOPSTATUS

fi

else

#If they didnt choose to return to stock we set the variable to include their radio selections.
if [ -z "$ARDOPCMD" ]; then
ARDOPCMD="$ARDOPLOC$ONOFF$DEV &"
fi
if [[ $RIGCTLSEL == No ]];then
RIGCTLCMD='echo "You did not select to setup rigctld" | yad --center --text-info --wrap --title="Not Configured" --height=100 --width=300'
fi
fi
}



function vox_setup() {
ONOFF=" 8515 pulse pulse"
DEV=""

}

function rts_setup() {
DEV=$(yad --center --title="TriMode Setup" --text="Please answer the following selections:" \
--image=$DIR/images/corps-icon.png \
--form --separator="," --item-separator="," \
--field="PTT Device::CB" $RTSOPTIONS ${c} ${r}) || exit 1

ONOFF=" 8515 pulse pulse"
DEV=$(echo $DEV | awk -F "," '{print $1}')
DEV=" -p /dev/$DEV"
}

function cat_setup() {


CATSEL=$(yad --center --title="TriMode Setup" --text="Please answer the following selections:" \
--image=$DIR/images/corps-icon.png \
--form --separator="," --item-separator="," \
--field="Select CAT device::CB" $RTSOPTIONS \
--field="Enter Baud Rate" "19200" \
--field="Select Radio::CB" $RADIOS ${c} ${r}) || exit 1

CATDEV=$(echo $CATSEL | awk -F "," '{print $1}')
CATBAUD=$(echo $CATSEL | awk -F "," '{print $2}')
RADIOSEL=$(echo $CATSEL | awk -F "," '{print $3}')
case $RADIOSEL in

FT-817)
k="-k 0000000008"
u="-u 0000000088"
;;
FT-857)
k="-k 0000000008"
u="-u 0000000088"
;;
FT-891)
k='-k "TX1;"'
u='-u "TX0;"'
;;
FT-897)
k="-k 0000000008"
u="-u 0000000088"
;;
FT990)
k="-k 000000010F"
u="-u 000000000F"
;;
FTDX-3000)
k='-k "TX1;"'
u='-u "TX0;"'
;;
IC-706Mk2G)
k="-k FEFE58E01C0001FD"
u="-u FEFE58E01C0000FD"
;;
IC-7100)
k="-k FEFE88E01C0001FD"
u="-u FEFE88E01C0000FD"
;;
IC-7200)
k="-k FEFE76E01C0001FD"
u="-u FEFE76E01C0000FD"
;;
IC-7300)
k="-k FEFE94E01C0001FD"
u="-u FEFE94E01C0000FD"
;;
IC-7410)
k="-k FEFE80E01C0001FD"
u="-u FEFE80E01C0000FD"
;;
esac

ONOFF=" 8515 pulse pulse -c"

DEV=" /dev/$CATDEV:$CATBAUD $k $u"
}


function socat_setup() {

 if ! hash socat 2>/dev/null; then
     sudo apt install -y socat
 fi

ONOFF=" 8515 pulse pulse"
DEV=" --cat /tmp/rigctl:38400 --keystring 5420310A --unkeystring 5420300A"

#Set a variable flag so we can use remember this was selected later.
SOCATRIGCTL="True"



}


function show_results() {

cat /etc/asound.conf | yad --center --text-info --image=$DIR/images/corps-icon.png --wrap --title=TriMode --button="gtk-ok:0" --text="Your asound.conf file has been created with the following information." ${c} ${r}

awk 'NR==92 || NR==119 || NR==120' $ARDOPSTATUS | yad --center --text-info --image=$DIR/images/corps-icon.png --wrap --title=TriMode --button="gtk-ok:0" --text="Your TNC Status launcher command has been modified to the following.." ${c} ${r}


}

function final_instructions() {

echo "Tri-Mode has been configured.

Procedure:

1. Launch FLRIG, configure transceiver for your setup.
2. Launch the ardop-status app.
3. Enable RIGTLD and PAVUCONTROL, select Toggle. They should now have PID's listed.
4. You can now enable ARDOPC and ARDOP_GUI.

*NOTE* If you wish to use PAT you must first detach the TNC from ARIM before loading PAT." | yad --image=$DIR/images/corps-icon.png \
--text-info --center --button="gtk-ok:0" \
--wrap \
--title "Complete" ${r} ${c}


}


function info_dialogs() {

echo "This will create an asound.conf file in the /etc/ directory with pulse audio settings and modify the ardop-status launcher in /usr/local/bin for your selected configuration.

This script is meant to aid you in configuring but cannot account for all variations.

RIGCTL does not play nicely with CAT and RTS keying across all software.  If you wish to use RIGCTL it is recommended to select SOCAT and check the RIGCTL box on the next screen." | yad --center --text-info --image=$DIR/images/corps-icon.png --wrap --title=TriMode ${c} ${r} || exit 1
}

function selection_dialogs() {
##This creates our menu with the initial selections and outputs the results to /tmp/trimode-selections
yad --center --wrap --title="TriMode Setup" --text="Please answer the following selections:" \
--image=$DIR/images/corps-icon.png \
--form --separator="," --item-separator="," --align=left \
--field="Select ardop file:SFL" $HOME/Radio/ARDOP \
--field="PTT Method::"CB "SOCAT-RIGCTLD,VOX,RTS,CAT" \
--field="Configure RIGCTLD?  ***Must select with SOCAT***:"CB "No,Yes" \
--field="Tip::TXT" "Please ensure you select your ardop file in the first option.

If you want to use rigctl in ARDOP along with your other programs -- Select SOCAT.
CAT and RTS options will conflict when running rigctl.

The below option returns your TNC-Status launcher to launching ardop radio specific arguments." \
--field="Return Status launcher to Defaults?::"CB "No,Yes" \
${c} ${r} > /tmp/trimode-selections || exit 1

#Here we read the selections and pull out the relevant info from them.
ARDOPLOC=$(cat /tmp/trimode-selections | awk -F "," '{print $1}')
PTT=$(cat /tmp/trimode-selections | awk -F "," '{print $2}')
STOCK=$(cat /tmp/trimode-selections | awk -F "," '{print $5}')
RIGCTLSEL=$(cat /tmp/trimode-selections | awk -F "," '{print $3}')

#This catches if the user failed to select the ardopc location and sends them back to try again
if [[ ! -f $ARDOPLOC ]]; then
	echo "You must select your ardop file before continuing." | yad --center --text-info --wrap --title=TriMode --button="Start Over:1" ${c} ${r} ||
	{ selection_dialogs; return; }
  fi

#If they did not elect to return the status launcher back to stock then we continue on.
if [[ $STOCK == No ]]; then
#Run the appropriate function depending on what mode they selected.
case $PTT in
	VOX)
	vox_setup
	;;
	RTS)
	rts_setup
	;;
	CAT)
	cat_setup
	;;
    	SOCAT-RIGCTLD)
    	socat_setup
    	;;
  esac
#If they checked to enable rigctl then run that function before going to status update and creating the new launcher.
if [[ $RIGCTLSEL == Yes ]]; then
rigctl_setup
fi
status_update
ardop_launcher
	else #If they elected to return the launcher to stock, skip everything and just apply the status update function
	status_update
  fi

}

function easy_button() {

	echo "Would you like to use the default Tri-Mode setup?  This setup will use FLRIG to control your radio and allow for Tri/Quad Mode easily for most popular setups." | yad \
--center \
--text-info \
--image=$DIR/images/corps-icon.png \
--wrap \
--title="Easy Button" \
--button="Yes:0" \
--button="Custom Setup:1" \
${c} ${r} ||
        { selection_dialogs; }

 if ! hash socat 2>/dev/null; then
     sudo apt install -y socat
 fi

yad --center --wrap --title="ARDOP Location" --text="Please answer the following selections:" \
--image=$DIR/images/corps-icon.png \
--form --separator="," --item-separator="," --align=left \
--field="Select ardop file:SFL" $HOME/Radio/ARDOP \
--field="Tip::TXT" "Please ensure you select your ardop file." \
${c} ${r} > /tmp/trimode-selections || exit 1

#Here we read the selections and pull out the relevant info from them.
ARDOPLOC=$(cat /tmp/trimode-selections | awk -F "," '{print $1}')

#This catches if the user failed to select the ardopc location and sends them back to try again
if [[ ! -f $ARDOPLOC ]]; then
	echo "You must select your ardop file before continuing." | yad --center --text-info --wrap --title=TriMode --button="Start Over:1" ${c} ${r} ||
	{ easy_button; return; }
  fi

RIGCTLCMD="socat pty,link=/tmp/rigctl,waitslave,b38400 tcp:localhost:4532,retry &
rigctld -vvvvv -m 4 &"
ARDOPCMD="$ARDOPLOC 8515 pulse pulse --cat /tmp/rigctl:38400 --keystring 5420310A --unkeystring 5420300A &"
status_update
ardop_launcher
}


set_screen_size
determine_os
info_dialogs
easy_button
create_asound
show_results
final_instructions
