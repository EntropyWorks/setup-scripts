# AmRRON Setup Scripts

# Installation

You will need git to clone this repository on your system.  Install git with:

    sudo apt install git

Now clone the repository to your local system:

    git clone https://gitlab.com/amrron/setup-scripts.git

Move into the new directory:

    cd setup-scripts 

To get updates to the scripts, ensure you are in the setup-scripts directory and run:

    git pull

# Running the scripts

To run a script simply type:

    ./filename

Alternatively, a bash 'debug' mode can be used to see what the script is doing by running with:

    bash -x filename

If the script is not already executable, then run:
    
    chmod +x filename

You could also run the script by double clicking and selecting 'Run in terminal' from you file browser but any errors will simply close the session and you won't know what errored or why.

If you run into errors, please attempt again and run with bash -x to see the output.  If you can provide the final section of that that shows the error along with your system information it will assist in troubleshooting.   Thank you.

# Common Errors

 - Hamlib and the FLDIGI suite are hosted on sourceforge.  Lately it appears sourceforge has periods of down time.  If the script is unable to download one of these programs or query sourceforge for the current version, you should receive an error message at the end of the install process.  These problems are usually fixed by simply re-running the setup script later and selecting the items that failed to download.
 - If Hamlib fails to be installed before FLDigi is compiled, FLDigi will be compiled without hamlib support.  If you need that functionality do the following after you successfully install hamlib:
 
        cd ~/Radio/fldigi-4.1.01   
                # Use the appropriate version number
 
        ./configure --enable-optimizations=native  
                # (For Pi, do not add the enable optimizations)
                # You should see hamlib support marked yes

        make && sudo make install

        

